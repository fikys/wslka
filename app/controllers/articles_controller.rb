
class ArticlesController < ApplicationController
	before_filter :authenticate_user!, :except => [:show, :index]	


	def destroy
		@article = Article.find(params[:id])
		if @article.author == current_user.username.capitalize 	
 		    @article.destroy
 		 	redirect_to '/'
 		else redirect_to @article
 		end
	end

	def update                            
		@article = Article.find(params[:id])
		if @article.author == current_user.username.capitalize 	
			if @article.update(article_params)
	 			redirect_to @article
			else
				render action: 'edit'
			end
		else redirect_to @article
		end
	end

	def show
		@article = Article.find(params[:id])
	
	end

	def edit
		@article = Article.find(params[:id])
	end

	def new
	end

	def create
		@article = Article.new(article_params)
		@article.author = current_user.username.capitalize
		if @article.valid?
				
			#	if @article.text.include? "src=" 
			 #       	@article.text.insert(@article.text.index('src='),'class="pickta" ')	
			  #      end
			  
			@article.save
			render action: 'show'
		else
			render action: 'new'
		end

	end

	private 
	def article_params
	params.require(:article).permit(:title, :text, :image, :original)

	end		
	

end
