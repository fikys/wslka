json.array!(@attachments) do |attachment|
  json.extract! attachment, :id, :image_uid
  json.url attachment_url(attachment, format: :json)
end
