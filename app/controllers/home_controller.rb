
class HomeController < ApplicationController
  def index
  	@main_article = Article.last
	@small_articles = Article.last(4).reverse.slice(1..3)
	@last_articles = Article.last(16).reverse.slice(4..16)
  end
	
end
