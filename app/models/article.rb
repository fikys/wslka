class Article < ActiveRecord::Base
	dragonfly_accessor :image
	validates :title, presence: true
	validates :text, presence: true
	validates :image, presence: true
	
	
end
